//
//  WorkerTableViewCell.swift
//  Dwarf pool
//
//  Created by Anton Marunko on 22/06/2017.
//  Copyright © 2017 company. All rights reserved.
//

import UIKit
import RxSwift

class WorkerTableViewCell: UITableViewCell {

    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var hashRateLabel: UILabel!
    @IBOutlet var activeView: UIView!
    
    private var nameSubscribe:Disposable?
    private var hashRateSubscribe:Disposable?
    private var activeSubscribe:Disposable?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        activeView.backgroundColor = UIColor.red
        activeView.layer.cornerRadius = activeView.frame.size.width / 2;
    }
    
    var viewModel: WorkerCellViewModel? = nil
    
    func setViewModel(viewModel:WorkerCellViewModel) {
        self.viewModel = viewModel
        
        setFieldsBinding()
    }
    
    func setFieldsBinding() {
        nameSubscribe = self.viewModel!.name.subscribe(onNext: {[unowned self] results in
            
            self.nameLabel.text = NSLocalizedString("name: ", comment: "") + results
        })
        
        hashRateSubscribe = self.viewModel!.hashRate.subscribe(onNext: { [unowned self] value in
            self.hashRateLabel.text = NSLocalizedString("Hashrate: ", comment: "") + String(value)
        })
        
        activeSubscribe = self.viewModel!.active.subscribe(onNext: { [weak self] active in
            self?.setActive(active: active)
        })
    }
    
    private func setActive(active: Bool) {
        if active {
            activeView.backgroundColor = UIColor.green
        } else {
            activeView.backgroundColor = UIColor.red
        }
    }
    
    override func prepareForReuse()
    {
        super.prepareForReuse()
        
        self.nameSubscribe?.dispose()
        self.hashRateSubscribe?.dispose()
        self.activeSubscribe?.dispose()
    }
}
