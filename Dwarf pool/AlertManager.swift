//
//  AlertManager.swift
//  Dwarf pool
//
//  Created by Anton Marunko on 23/06/2017.
//  Copyright © 2017 company. All rights reserved.
//

import UIKit

fileprivate let delayForShowingAlert: TimeInterval = 2

class AlertManager: NSObject {

    static let shared = AlertManager()
 
    func showAlert(message: String)
    {
        self.showAlert(title: nil, message: message)
    }
    
    func showToast(title:String? = nil, message: String) {
        self.showAlert(title: title, message: message, isToast: true)
    }
    
    func showAlert(title:String?, message: String?, isToast: Bool = false)
    {
        let topWindow: UIWindow = UIWindow(frame: UIScreen.main.bounds)
        topWindow.rootViewController = UIViewController()
        topWindow.windowLevel = UIWindowLevelAlert + 1
        
        let alertController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if isToast {
            DispatchQueue.main.asyncAfter(deadline: .now() + delayForShowingAlert) {
                topWindow.isHidden = true
            }
        } else {
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel) { (action: UIAlertAction) in
                topWindow.isHidden = true
            })
        }
        
        topWindow.makeKeyAndVisible()
        topWindow.rootViewController!.present(alertController, animated: true, completion: nil)
    }
    
    func showConfirm(title:String?, message: String?, okAction: @escaping () -> Void)
    {
        let topWindow: UIWindow = UIWindow(frame: UIScreen.main.bounds)
        topWindow.rootViewController = UIViewController()
        topWindow.windowLevel = UIWindowLevelAlert + 1
        
        let alertController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: { (action:UIAlertAction) in
            
            okAction()
        }))
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .default) { (action: UIAlertAction) in
            topWindow.isHidden = true
        })
        
        topWindow.makeKeyAndVisible()
        topWindow.rootViewController!.present(alertController, animated: true, completion: nil)
    }
}
