//
//  Worker.swift
//  Dwarf pool
//
//  Created by Anton Marunko on 22/06/2017.
//  Copyright © 2017 company. All rights reserved.
//

import UIKit
import Gloss

struct WorkerRepository: Decodable {
    let name: String?
    let hashrate: Double?
    let hashrateBelowThreshold: Bool?
    let hashrateCalculated: Double?
    let lastSubmit: Date?
    let secondSinceSubmit: Int?
    let isAlive: Bool?
    
    private let lastSubmitDateFormat = "EEE, dd MMM YYYY HH:mm:ss zzz"
    
    // MARK: - Deserialization
    
    init?(json: JSON) {
        
        self.name = "worker" <~~ json
        self.hashrate = "hashrate" <~~ json
        self.hashrateBelowThreshold = "hashrate_below_threshold" <~~ json
        self.hashrateCalculated = "hashrate_calculated" <~~ json
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = lastSubmitDateFormat
        
        self.lastSubmit = Decoder.decode(dateForKey: "last_submit", dateFormatter: dateFormatter)(json)
        self.secondSinceSubmit = "second_since_submit" <~~ json
        self.isAlive = "alive" <~~ json
    }
}
