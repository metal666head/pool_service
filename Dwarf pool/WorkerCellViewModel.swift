//
//  WorkerCellViewModel.swift
//  Dwarf pool
//
//  Created by Anton Marunko on 22/06/2017.
//  Copyright © 2017 company. All rights reserved.
//

import UIKit
import RxSwift

class WorkerCellViewModel: NSObject {
    public var name = BehaviorSubject<String>(value: "")
    public var hashRate = BehaviorSubject<Double>(value: 0.0)
    public var active = BehaviorSubject<Bool>(value: false)
    
    public func setName(name: String) {
        self.name.onNext(name)
    }
    
    public func setHashRate(hashrate: Double) {
        self.hashRate.onNext(hashrate)
    }
    
    public func setActive(active: Bool){
        self.active.onNext(active)
    }
}
