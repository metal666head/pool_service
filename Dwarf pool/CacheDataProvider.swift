//
//  CacheDataProvider.swift
//  Dwarf pool
//
//  Created by Anton Marunko on 21/07/2017.
//  Copyright © 2017 company. All rights reserved.
//

import Foundation

class CacheDataProvider {
    
    class var defaultWallet: String? {
        get {
            return UserDefaults.standard.value(forKey: walletDefaultsKey) as? String
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: walletDefaultsKey)
        }
    }
    
    class var defaultEmail: String? {
        get {
            return UserDefaults.standard.value(forKey: emailDefaultsKey) as? String
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: emailDefaultsKey)
        }
    }
}
