//
//  WorkersListRouter.swift
//  Dwarf pool
//
//  Created by Anton Marunko on 22/06/2017.
//  Copyright © 2017 company. All rights reserved.
//

import UIKit

fileprivate let loginControlIdentifier = "loginControl"
fileprivate let detailControlIdentifier = "workerDetailControl"

@objc protocol WorkersListRoutingLogic
{
    func routeToShowDetail(segue: UIStoryboardSegue)
    func showLogin ()
}

class WorkersListRouter: NSObject, WorkersListRoutingLogic {
    weak var controller: UIViewController?
    
    func showLogin () {
        if controller != nil {
            let storyBoard = UIStoryboard.init(name: mainStoryBoardName, bundle: nil)
            
            let loginControl = storyBoard.instantiateViewController(withIdentifier: loginControlIdentifier)
            
            controller?.present(loginControl, animated: true, completion: nil)
        }
    }
    
    func routeToShowDetail(segue: UIStoryboardSegue)
    {
        let sourceVC = segue.source as! WorkersListViewController
        let destinationVC = segue.destination as! WorkerDetailViewController
        passDataToShowDeatil(source: sourceVC, destination: destinationVC)
    }
    
    func passDataToShowDeatil(source: WorkersListViewController, destination: WorkerDetailViewController)
    {
        let selectedRow = source.tableView.indexPathForSelectedRow?.row
        if let worker = source.viewModel.workers?[selectedRow!] {
            destination.workerRepository = worker
        }
    }
}
