//
//  WokersListViewModel.swift
//  Dwarf pool
//
//  Created by Anton Marunko on 22/06/2017.
//  Copyright © 2017 company. All rights reserved.
//

import UIKit
import RxSwift
import Gloss

class WorkersListViewModel: NSObject {
    var router: (WorkersListRoutingLogic & NSObjectProtocol)
    init(router: WorkersListRouter) {
        self.router = router
        
        super.init()
    }
    
    var disposeBag = DisposeBag()
    
    public var refreshSubject = PublishSubject<Void>.init()
    public var workers: [WorkerRepository]? = nil
    public var workersModels = Variable<[WorkerCellViewModel]>([WorkerCellViewModel]())
    public var isLoading: BehaviorSubject<Bool> = BehaviorSubject.init(value: false)
    
    //MARK: - Subscribe
    
    public var data: Disposable?
    
    func subscribeToData() {
        unsubscribeFromData()
        if let wallet = CacheDataProvider.defaultWallet, let mail = CacheDataProvider.defaultEmail {
                isLoading.onNext(true)
            data = DataProvider.shared.getGeneralInfo(with: wallet, email: mail).subscribe(onNext: {[unowned self] value in
                self.isLoading.onNext(false)
                
                self.parseWorkerModels(with: value?.workers)
            })
            data?.addDisposableTo(disposeBag)
        } else {
            router.showLogin()
        }
        
        refreshSubject.onNext()
    }
    
    func unsubscribeFromData() {
        data?.dispose()
        data = nil
    }
    
    //MARK: - LifeCycle
    
    public func onViewWillAppear() {
        subscribeToData()
    }
    
    public func onViewDisappear() {
        unsubscribeFromData()
    }
    
    //MARK: - Parsing
    
    public func workerCellViewModel(with worker: WorkerRepository) -> WorkerCellViewModel {
        let model = WorkerCellViewModel()
        model.setName(name: worker.name!)
        model.setActive(active: worker.isAlive!)
        model.setHashRate(hashrate: worker.hashrate!)
        
        return model
    }
    
    func parseWorkerModels(with workersEntities: [WorkerRepository]?) {
        self.workers = workersEntities
        
        var workerModels: [WorkerCellViewModel] = []
        if self.workers != nil {
            for worker in self.workers! {
                let model = self.workerCellViewModel(with: worker)
                workerModels.append(model)
            }
        }
        
        self.workersModels.value = workerModels
    }
    
    public func refreshData() {
        self.subscribeToData()
    }
}
