//
//  DataProvider.swift
//  Dwarf pool
//
//  Created by Anton Marunko on 23/06/2017.
//  Copyright © 2017 company. All rights reserved.
//

import UIKit
import RxSwift
import Moya

class DataProvider: NSObject {
    static let shared = DataProvider()
    var disposeBag = DisposeBag()
    fileprivate var provider = RxMoyaProvider<NetworkService>()
    
    override init() {
        generalInfoRepository = BehaviorSubject<GeneralInfoRepository?>.init(value: nil)
        super.init()
    }
    
    var generalInfoRepository: BehaviorSubject<GeneralInfoRepository?>
    
    public func getGeneralInfo(with wallet: String, email: String? = defaultEmail) -> Observable<GeneralInfoRepository?>{
        provider.request(.generalInfo(walletId: wallet, email: email!)).subscribe(onNext: { [weak self] response in
            
            if let successResponse = try? response.filterSuccessfulStatusCodes() {
                if let json = try? JSONSerialization.jsonObject(with: successResponse.data, options: .allowFragments) {
                    guard let dictionary = json as? [String: Any] else {
                        return
                    }
                    
                    let generalInfo = GeneralInfoRepository.init(json: dictionary)
                    
                    if let apiError = generalInfo?.errorCode {
                        var alertMessage = NSLocalizedString("Sorry, something goes wrong, please, try again later", comment: "")
                        switch apiError {
                        case .ApiDown:
                            alertMessage = NSLocalizedString("Sorry, api on maintenance now, please, try again later ", comment: "")
                        case .BadWallet:
                            alertMessage = NSLocalizedString("Wrong wallet, please, enter another wallet value", comment: "")
                        case .NoAuth:
                            alertMessage = NSLocalizedString("Wrong email, please, enter another email value or live email field empty", comment: "")
                        case .NoStat:
                            alertMessage = NSLocalizedString("Sorry, no statistic for wallet. Either new wallet or no active workers last day.", comment: "")
                        }
                        
                        AlertManager.shared.showAlert(message: alertMessage)
                        
                    } else {
                        self?.generalInfoRepository.onNext(generalInfo)
                    }
                }
            }
            
            }, onError: { error in
                AlertManager.shared.showAlert(message: error.localizedDescription)
        }).addDisposableTo(disposeBag)
        
        return generalInfoRepository.asObservable()
    }
}
