//
//  LoginRouter.swift
//  Dwarf pool
//
//  Created by Anton Marunko on 22/06/2017.
//  Copyright © 2017 company. All rights reserved.
//

import UIKit

class LoginRouter: NSObject {
    
    weak var controller: UIViewController?
    
    func doDataSubmit() {
        controller?.dismiss(animated: true, completion: nil)
    }
}
