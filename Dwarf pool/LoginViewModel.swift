//
//  LoginViewModel.swift
//  Dwarf pool
//
//  Created by Anton Marunko on 22/06/2017.
//  Copyright © 2017 company. All rights reserved.
//

import UIKit
import RxSwift

class LoginViewModel: NSObject {
    public var walletString:Variable<String> = Variable.init("")
    public var emailText:Variable<String> = Variable.init("")
    var interactor: LoginRouter
    
    init(interactor: LoginRouter) {
        self.interactor = interactor
        
        super.init()
    }
    
    func onLSubmitButtonClick() {
        if checkFieldsValues() {
            var mail = emailText.value
            if mail.isEmpty {
                mail = defaultEmail
            }
            
            CacheDataProvider.defaultWallet = walletString.value
            CacheDataProvider.defaultEmail = mail
            interactor.doDataSubmit()
        }
    }
    
    func checkFieldsValues() -> Bool {
        
        if !emailText.value.isEmpty && !emailText.value.isValidEmail() {
            AlertManager.shared.showAlert(title:NSLocalizedString("Invalid email", comment: ""), message: NSLocalizedString("Email address is not valid", comment: ""))
            
            return false
        }
        
        if walletString.value.isEmpty {
            AlertManager.shared.showAlert(title: NSLocalizedString("Invalid wallet value", comment: ""), message: NSLocalizedString("Walllet is empty", comment: ""))
            
            return false
        }
        
        return true
    }
}
