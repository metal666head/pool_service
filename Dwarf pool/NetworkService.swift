//
//  DwarfService.swift
//  Dwarf pool
//
//  Created by Anton Marunko on 22/06/2017.
//  Copyright © 2017 company. All rights reserved.
//

import Foundation
import Moya

enum NetworkService {
    case generalInfo(walletId: String, email: String)
}

extension NetworkService: TargetType {
    var baseURL: URL { return URL(string: "http://dwarfpool.com/eth/api")! }
    
    var path: String {
        switch self {
        case .generalInfo:
            return ""
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .generalInfo:
            return .get
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .generalInfo(let walletId, let email):
            return ["wallet": walletId, "email": email]
        }
    }
    
    var parameterEncoding: ParameterEncoding {
        switch self {
        case .generalInfo:
            return URLEncoding.queryString
        }
    }
    
    var task: Task {
        switch self {
        case .generalInfo:
            return .request
        }
    }
    
    var sampleData: Data {
        switch self {
        case .generalInfo:
            return "{\"error\": \(false), \"total_hashrate\": \"146.8\", \"total_hashrate_calculated\": \"147.47\"}".utf8Encoded
        }
    }
}
