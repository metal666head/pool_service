//
//  LoginViewController.swift
//  Dwarf pool
//
//  Created by Anton Marunko on 22/06/2017.
//  Copyright © 2017 company. All rights reserved.
//

import UIKit
import RxSwift

infix operator <-> : DefaultPrecedence

class LoginViewController: UIViewController {
    
    @IBOutlet weak var walletTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    
    let viewModel: LoginViewModel;
    var disposeBag: DisposeBag = DisposeBag()
    
    required init?(coder aDecoder: NSCoder) {
        
        let interactor = LoginRouter()
        viewModel = LoginViewModel(interactor: interactor)
        
        super.init(coder: aDecoder)
        
        interactor.controller = self
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupBindings()
    }
    
    //MARK: - Setup
    
    private func setupBindings() {
        
        (self.emailTextField.rx.textInput <-> self.viewModel.emailText).addDisposableTo(disposeBag)
        (self.walletTextField.rx.textInput <-> self.viewModel.walletString).addDisposableTo(disposeBag)
        
        self.submitButton.rx.tap.subscribe{ [weak self] (event) -> Void in
            
            self?.viewModel.onLSubmitButtonClick()
            
            }.addDisposableTo(self.disposeBag)
        
    }

    //MARK: - Actions
    
    @IBAction func submitButtonPressed(_ sender: Any) {
        viewModel.onLSubmitButtonClick()
    }
}
