//
//  UIViewController+Loading.swift
//  Dwarf pool
//
//  Created by Anton Marunko on 01/06/2017.
//  Copyright © 2017 company. All rights reserved.
//
import UIKit

fileprivate let loadingTag = 1123002;
fileprivate let defaultTimeInterval: TimeInterval = 0.3
extension UIViewController {
    
    func createLoadingView() -> UIView {
        let loading = UIView.init()
        loading.backgroundColor = UIColor.clear
        loading.tag = loadingTag
        loading.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(loading)
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[loading]|", options: NSLayoutFormatOptions.init(rawValue: 0), metrics: nil, views: ["loading":loading]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[loading]|", options: NSLayoutFormatOptions.init(rawValue: 0), metrics: nil, views: ["loading":loading]))
                               
        let activity = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
        activity.startAnimating()
        activity.translatesAutoresizingMaskIntoConstraints = false
        
        loading.addSubview(activity)
        
        let centerX: NSLayoutConstraint = NSLayoutConstraint(item: activity, attribute: .centerX, relatedBy: .equal, toItem: loading, attribute: .centerX, multiplier: 1.0, constant: 0.0)
        let centerY: NSLayoutConstraint = NSLayoutConstraint(item: activity, attribute: .centerY, relatedBy: .equal, toItem: loading, attribute: .bottom, multiplier: 0.5, constant: 0.0)
        
        
        loading.addConstraints([centerX, centerY])
        loading.setNeedsUpdateConstraints()
        loading.layoutIfNeeded()
        
        self.view.bringSubview(toFront: loading)
        
        return loading
    }
    
    func loading(visible:Bool){
        UIApplication.shared.isNetworkActivityIndicatorVisible = visible
        
        var loadingView:UIView? = self.view.viewWithTag(loadingTag)
        
        if loadingView == nil {
            if !visible {
                return
            }
            
            loadingView = self.createLoadingView()
        }
        
        if !visible && loadingView!.isHidden {
            return
        }
        
        loadingView!.isUserInteractionEnabled = !visible
        
        if(visible) {
            loadingView!.isHidden = false
            self.view.bringSubview(toFront: loadingView!)
        }
        
        loadingView!.alpha = visible ? 0 : 1
        
        DispatchQueue.main.async {
            
            UIView .animate(withDuration: defaultTimeInterval, animations: {
                
                loadingView!.alpha = visible ? 1 : 0
            },
            completion: { (finished:Bool) in
                
                if(!visible) {
                    loadingView!.isHidden = true
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
            })
        }
    }
}
