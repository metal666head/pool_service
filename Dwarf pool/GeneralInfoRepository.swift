//
//  GeneralInfoRepository.swift
//  Dwarf pool
//
//  Created by Anton Marunko on 22/06/2017.
//  Copyright © 2017 company. All rights reserved.
//

import Foundation
import Gloss

struct GeneralInfoRepository: Decodable {
    
    enum ErrorCode: String {
        case ApiDown = "API_DOWN"
        case BadWallet = "BAD_WALLET"
        case NoAuth = "NO_AUTH"
        case NoStat = "NO_STAT"
    }
    
    let error: Bool?
    let errorCode: ErrorCode?
    let totalHashrate: Double?
    let totalHashrateCalculated: Double?
    let wallet: String?
    let workers: [WorkerRepository]?
    
    // MARK: - Deserialization
    
    init?(json: JSON) {
        self.error = "error" <~~ json
        self.errorCode = "error_code" <~~ json
        self.totalHashrate = "total_hashrate" <~~ json
        self.totalHashrateCalculated = "total_hashrate_calculated" <~~ json
        self.wallet = "wallet" <~~ json
        
        var workersList: [WorkerRepository] = []
        
        if let dictionary = json.valueForKeyPath(keyPath: "workers") as? Dictionary<String, Any> {
            for key in dictionary.keys {
                let worker: WorkerRepository? = key <~~ dictionary
                
                if let decodedWorker = worker {
                    workersList.append(decodedWorker)
                }
                
            }
        }
        
        self.workers = workersList
    }
}
