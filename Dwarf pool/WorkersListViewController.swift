//
//  WorkersListViewController.swift
//  Dwarf pool
//
//  Created by Anton Marunko on 22/06/2017.
//  Copyright © 2017 company. All rights reserved.
//

import UIKit
import RxSwift

fileprivate let workersListCellIdentifier = "workersListCell"

class WorkersListViewController: UIViewController {
    @IBOutlet var tableView: UITableView!

    let viewModel: WorkersListViewModel
    let refreshControl = UIRefreshControl.init()
    required init?(coder aDecoder: NSCoder)
    {
        let router = WorkersListRouter.init()
        self.viewModel = WorkersListViewModel.init(router: router)
        
        super.init(coder: aDecoder)
        
        router.controller = self;
    }
    
    //MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupRefreshControl()
        setupBindings()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.onViewWillAppear()
        
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at:selectedIndexPath, animated: true)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        viewModel.onViewDisappear()
        
        super.viewDidDisappear(animated)
    }
    
    //MARK: - Setup
    
    func setupRefreshControl() {
        tableView.refreshControl = refreshControl
        tableView.refreshControl?.addTarget(self, action: #selector(refreshTriggered), for: UIControlEvents.valueChanged)
    }
    
    func refreshTriggered() {
        self.viewModel.refreshData()
    }
    
    var disposeBag = DisposeBag()
    
    func setupBindings() {
        self.viewModel.isLoading.subscribe(onNext: { [weak self] (value:Bool) in
            
            if self != nil {
                if value {
                    self!.loading(visible: true)
                }
                else {
                    self!.loading(visible: false)
                }
            }
            
        }).addDisposableTo(disposeBag)
        
        self.viewModel.workersModels.asObservable().skip(1).subscribe(onNext: { [unowned self] models in
            self.tableView.reloadData()
        }).addDisposableTo(disposeBag)
        
        self.viewModel.refreshSubject.subscribe(onNext: { [unowned self] in
            self.refreshControl.endRefreshing()
        }).addDisposableTo(disposeBag)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if viewModel.router.responds(to: selector) {
                viewModel.router.perform(selector, with: segue)
            }
        }
    }
}

extension WorkersListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.workersModels.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: workersListCellIdentifier, for: indexPath) as! WorkerTableViewCell
        let cellViewModel = viewModel.workersModels.value[indexPath.row]
        cell.setViewModel(viewModel: cellViewModel)
        
        return cell
    }
}

extension WorkersListViewController: UITableViewDelegate {
    
}
